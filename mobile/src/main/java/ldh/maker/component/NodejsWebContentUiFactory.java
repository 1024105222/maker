package ldh.maker.component;

/**
 * Created by ldh on 2017/4/6.
 */
public class NodejsWebContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new NodejsWebContentUi();
    }
}
