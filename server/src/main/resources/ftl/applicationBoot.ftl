package ${projectRootPackage};

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
// import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
// @ComponentScan(basePackages={"${projectRootPackage}"})
public class ApplicationBoot { //extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationBoot.class, args);
    }
}
