package ${basePackage}.configuration;

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.TemplateHashModel;
import ldh.common.util.PaginationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class FreemarkerConfig {

    @Autowired
    protected freemarker.template.Configuration configuration;

    @PostConstruct
    public void  setSharedVariable(){
        BeansWrapperBuilder builder = new BeansWrapperBuilder(freemarker.template.Configuration.VERSION_2_3_23);
        builder.setUseModelCache(true);
        builder.setExposeFields(true);

        configuration.setClassicCompatible(true);
        configuration.setNumberFormat("#");

        // Get the singleton:
        BeansWrapper beansWrapper = builder.build();
        TemplateHashModel staticModels = beansWrapper.getStaticModels();
        TemplateHashModel fileStatics = null;
        try {
            fileStatics = (TemplateHashModel) staticModels.get(PaginationUtil.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fileStatics != null) {
            configuration.setSharedVariable("PaginationUtil", fileStatics);
        }
    }
}
